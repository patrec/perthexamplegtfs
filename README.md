# Sample GTFS Data for Testing

This repository contains hand-crafted GTFS data for use with testing ADAPT with
a very lightweight example. It was built based upon Google's spec available at
https://developers.google.com/transit/gtfs/reference and is compatible with the
'Aubin Grove Test Data' available in the ADAPTWebCore repository.

In short, this describes a transport network with one route between two stops
on New England Vista (in the north region of the test data) and The Horseshoe
(in the south region of the test data). The route runs daily via the most
direct routing and goes from North to South (8:00AM - 8:10AM, 5:00PM - 5:10PM)
twice a day and South to North (same times). The trips only run on weekdays.

When used with the test region data in ADAPTWebCore this can be used to provide
a very lightweight test to ADAPT to see if OpenTripPlanner is running and is
connected properly.

## Metadata

Last edited by Tristan Reed on 10 Feb 2021.
